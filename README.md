# antimixxx

Respin of antiX GNU/Linux to run the pro music player software mixxx on a realtime kernel with the minimum of infrastructure, for better performance (in terms of latency). No pulseaudio (no systemd either, nor avahi) and no daemons for network configuration ("ceni" available for configuration).

antiX can be run as a live distribution from CD or USB, as a normal HD installation, or in the interesting frugal mode (live residing on HD). See http://antixlinux.com for details. 

**IMPORTANT: THIS IS A PERSONAL PROJECT/ ALPHA QUALITY, YOU ARE ON YOUR OWN, USE IT AT YOUR OWN RISK, NO WARRANTIES EITHER EXPRESSED OR IMPLIED, BLAH BLAH**

A respin consists in minor modifications to a linux distribution, so: **THIS IS NOT AN OFFICIAL ANTIX VERSION, which means it is UNSUPPORTED by the ANTIX team**.

**DOWNLOAD**



USB image:

https://www.dropbox.com/s/b5dlwonckf5tqy8/snapshot-20200415_0335.img.gz?dl=0

Checksums (md5, sha512 respectively):

01141042f196b62791b08de0d3e7b208  snapshot-20200415_0335.img.gz

abc2dfd72789372275e51a8b82556d07c12ad2c9c740c4909779ea95b60acab955c362c7f197e1d6518a9484b0a94d5c3a81a47e7f509ca7a836d38d9b605069  snapshot-20200415_0335.img.gz

Antimixxx is also available as ISO image in case booting from usb is undesirable:

https://www.dropbox.com/s/v62h4ana51vu8f8/snapshot-20200415_0335.iso?dl=0

Checksums (md5, sha512 respectively):

d778899feb64c96d769037eeef91ef4e  snapshot-20200415_0335.iso

2da2643976a68ad7ce2c96b95b4e4a786d73b93d33e9a3603f391660222608e5b1ac6cb1983103d085dc6b0198006f744ef7503d1a3f07414935ce9075a3726a  snapshot-20200415_0335.iso

For security reasons, during booting the system asks you for the passwords for the user account (username is 'demo') and root. 
This happens when 'private' is part of the boot command line, see antix docs. In case they don't get asked, the default ones are "**antimixxx**".

**HOW TO INSTALL THE USB IMAGE ON A USB STICK**

To install, from GNU/Linux, get a usb stick with at least 2 GB capacity (no matter the capacity, it will get COMPLETELY erased BTW, make sure you KNOW on which device it is mounted (fake example, /dev/sdX), and from a terminal issue:

`sudo umount /dev/sdX*`

`sudo zcat snapshotXXXXX.img.gz > /dev/sdX`

After a couple minutes of utter silence from the terminal, the cursor will return and you will either have erased one of your disks if you got the device name wrong, or copied the image to the now bootable stick. 

Issue `sync` to be sure all of the data has been transferred. If you are more familiar with the tool dd, you can use that too.

To create the stick from windows you should seek help online on how to make a usb stick out of an img file. Try to uncompress (extract) the file into an .img then use a disk imager such as https://sourceforge.net/projects/win32diskimager/files/

**IMPORTANT note on LIVE Linux**: Booting from the USB stick or running the CD or in some other modes like frugal gives you a LIVE linux. That means, unless you choose some persistent options or modes (see antiX docs), most of the things you do to the system and your home directory stay in RAM and will DISAPPEAR when you reboot or crash. Why bother then? Because a live distribution RESTARTS ALWAYS IN THE SAME CONDITION, and this is ideal for people who know what they are doing and need mixxx on stage and don't want surprises.

Use gparted or similar if you want to reclaim some unused space on the stick (for experts). To boot from USB stick you need to look for specific instructions for your machines and optionally meddle with BIOS settings.

For better performance **SPECTRE MITIGATIONS ARE OFF BY DEFAULT**, so use the second option when booting if you want a safer system to use online.

**Building your own version**

The respin can be obtained by running scripts on an antix installation and then mastering the result as an ISO image.

The current antiX version that scripts should be applied on **antiX-19.2-runit_x64-full.iso**

You can likely adopt the 19.2 x64 full version without runit, too. At the moment i notice runit version still uses some older init scripts.

**Problems?**

No support is available ATM, my suggestion is to try installing mixxx on antix and experiment with that. 

**Example, for advanced users**

Say we are running a deb based distribution (antix, mx, debian, ubuntu...) and we want to try out the script in a VM. We need 4gb ram and around 10gb of HD and 64 bit CPU. Better to have at least two CPU cores.

Download the antimixxx-setup.sh from this repository and the antiX-19.2-runit_x64-full.iso from [antixlinux.com](url)

Install a VM supervisor if you don't already have one. In a terminal window (that we call the host window) issue:

`apt install qemu qemu-system-gui qemu-utils qemu-system-x86`

Create the virtual HD image for the installation, must be at least 4G, 7G if you want to generate some mastered ISO

`qemu-img create -f qcow2 antimixxx.qcow2 8G`

If you want to master to USB stick, create also the image

`qemu-img create -f raw antimixxx-usb.img 2G`


Run the VM with cd and hd image hooked up (better pick safe xorg mode, the second choice in the menu IIRC)
Note that -smp 2 -M pc -cpu host are optional speedups, -m assigns memory to the VM

`qemu-system-x86_64 -smp 2 -M pc -cpu host --enable-kvm -m 2047 --device e1000,netdev=net0  --netdev user,id=net0,hostfwd=tcp::2022-:22 --cdrom antiX-19.2-runit_x64-full.iso -hda antimixxx.qcow2`

The above also opens up local port 2022 to ssh into the VM

Next, in the VM, use the antix installer (icon on the desktop) to install to HD, it will show the HD image as sda or hda. See antiX linux if you are stuck, as of now we are dealing with a standard antiX.
Note that if you want the "private" boot option to work you must use 'demo' as the user name.
Remember the passwords you pick during installation...

The installer will offer to reboot, instead shut the VM down manually from the VM itself, so we can reboot without cdrom and with usb stick image:

`qemu-system-x86_64 -smp 2 -M pc -cpu host --enable-kvm -m 2047 --device e1000,netdev=net0  --netdev user,id=net0,hostfwd=tcp::2022-:22 -hda antimixxx.qcow2 -device qemu-xhci,id=xhci -drive if=none,id=vstick,file=antimixxx-usb.img,format=raw -device usb-storage,bus=xhci.0,drive=vstick`

Tip: if (either now or at next reboot) X doesn't come up and you are stuck with a text prompt, login as root (with the root password you chose during installation, either locally, or by opening up a tty in the VM using sendkey, or using ssh) and do the following:

`X -configure`

`cp /root/xorg.conf.new /etc/X11/xorg.conf`

`/etc/init.d/slim restart`

Next, open a terminal in the VM and enable ssh (the password is what you picked during installation for root)

`su`

`/etc/init.d/ssh restart`

Next, transfer the script to the VM: in the host window, the terminal outside the VM that is, connect to the VM, using the username you picked during installation ("user" in this example)

`sftp -P 2022 user@localhost`

enter the password (the one you picked for the user account)

at the sftp> prompt issue:

`put antimixxx-setup.sh`

(You had downloaded it earlier)

Next, open a terminal in the VM to run the script

Examine it first, edit it first, check its comments, and ALWAYS CHECK WHATEVER YOU RUN AS ROOT in general.

Just in case the script has wrong permissions, make it executable. If you want to touch X configuration (a corner case) you must do it from ssh separately.

`chmod u+x antimixxx-setup.sh`

`su`

enter the password (the one you picked for root)

`./antimixxx-setup.sh`

The script will tell you at one point that a reboot is in order, say OK.

The script will use ceni to reconfigure the network, since we are in a VM just accept the settings by pressing enter a couple times.

Reboot (issue the command inside the VM) NOTE THAT AUDIO IS NOT ENABLED so mixxx and alsamixer won't work in the VM, and it makes zero sense to try, you want to test on the host. In fact, better to remove .mixxx folder in your home so that it configures itself better when you start it up in an actual system.

Now you can tweak the system further to your liking. Then, create an ISO out of the system (ISO snapshot, in the antiX menu, see antiX docs)

NOTE: mastering the ISO will LOSE the linux boot parameter threadirqs, necessary for the best mixxx performance, and others needed to disable spectre mitigations which may hamper performance, so, after installation you will need at least to add threadirqs to syslinux or grub command line, ideally:

`threadirqs noibrs noibpb nopti nospectre_v2 nospectre_v1 l1tf=off nospec_store_bypass_disable no_stf_barrier mds=off tsx=on tsx_async_abort=off mitigations=off`

(see https://make-linux-fast-again.com/)

Finally, either make a usb out of the iso using Live USB maker (see antiX docs, after you shut down the VM the antimixxx-usb.img contains the image ready to be transferred to an actual stick) or grab the iso using sftp, and use it for a grub based frugal install on HD, see antiX docs.

Tip for experts: to mount the usb image, which contains more than one partition, use losetup to bind the image to a loop device, partprobe the loop device, and loop mount the partitions.

**Mixxx backporting notes**

Optionally, before mastering the ISO, build a more current mixxx, the version in sid can be backported.

Enable the deb-src lines in /etc/apt/...

apt-get build-dep mixxx

apt-get install packaging-dev debian-keyring devscripts equivs

download from packages.debian.org the source packages for mixxx (orig.tar.gz, dsc... files)

dpkg-source -x mixxx_2.2.3~dfsg-1.dsc 

cd mixxx-2.2.3~dfsg/

dch --bpo

debuild -b -uc -us

transfer the debs to the VM and 

dpkg -i the mixxx and mixxx-data packages, not the debug version.