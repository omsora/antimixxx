#/bin/bash
alsain="$1" # 1st parameter, name of ALSA input as obtained by arecord -L, like "my_Audio_8-DJ_(Ch-C)"
icecast="$2" # 2nd parameter, the streaming url, in the format login:pass@IP:port/streamname, like "source:mypassword@100.2.1.1:8123/stream
ffmpeg -ac 2 -ar 44100 -f alsa -i "$alsain" -c:a aac -b:a 128k -ar 44100 -content_type audio/aac -vn -f adts "icecast://$icecast"
