#!/bin/bash
# Check if root
if ! [ $(id -u) = 0 ]; then
   echo "This script must be run as root, use sudo or su -c. In general, better check what the script does before running as root."
   exit 1
fi

set -ev

# The following optional section can be removed if you don't care about disabling services and stuff
# remove stuff, you can pick some to save by deleting them from the list
apt-get purge -y connman connman-vpn  avahi-daemon avahi-utils resolvconf nfs-common gpm hexchat nfs-common sane-utils simple-scan transmission screenlight-antix libreoffice* ofono blueman bluetooth bluez bluez-firmware bluez-obexd network-assistant luckybackup blueman blueman mps-youtube smtube guvcview claws-mail mtpaint gtkam gweled dosbox gnome-mahjongg conky-legacy-all
# having removed stuff, you may need the following stuff else dbus complains, you can put any 32 bit hexadecimal string, dunno what is for and dunno if duplicate ids are a problem
dbus-uuidgen --ensure
# having removed resolvconf, I fill in google's dns below, you can use whatever DNS works for you.
echo "nameserver 8.8.8.8" > /etc/resolv.conf
ceni
apt-get autoremove -y
# end of optional section

apt-get update
#uncomment line below if you want to upgrade now, not recommended if there is a lot of packages to upgrade
#apt-get upgrade -y
apt-get install mixxx sqlite3 yudit-common -y

# boot command line setting (for grub as of now, gets lost by remastering the iso)
# the only needed option below is threadirqs, the others DISABLE SAFETY FEATURES to make the kernel faster, mind that if you keep it on the network
echo GRUB_CMDLINE_LINUX=\"threadirqs noibrs noibpb nopti nospectre_v2 nospectre_v1 l1tf=off nospec_store_bypass_disable no_stf_barrier mds=off tsx=on tsx_async_abort=off mitigations=off \" >> /etc/default/grub

apt-get install linux-image-5.4.0-0.bpo.4-rt-amd64 -y

# assign username below manually, must be the account you run mixxx with, else no realtime scheduling will occur. Check if you have more than one user in /home 
username=$(basename /home/* ) # works only on freshly installed systems
echo $username - rtprio 99 >> /etc/security/limits.conf

# other tweaks
chown $username /home/$username -R #fixes inaccurate permissions
# it is better to remove services during installation, and mind boot options too, anyway this attempts to remove unneeded daemons
rm -f /etc/rc5.d/S*ssh
rm -f/etc/rc5.d/S*rsync 

# my live session needs X to be configured, run these commands after disabling X (/etc/init.d/slim stop) , if you have 3d cards you might want to configure X according to antiX docs separately
# /etc/init.d/slim stop
# X -configure
# cp /root/xorg.conf.new /etc/X11/xorg.conf
# /etc/init.d/slim start

# DONE, now reboot, optionally install latest mixxx backport, tweak your system disabling unused startup services and then remaster the live system AND update live kernel if you started from a live usb session.
