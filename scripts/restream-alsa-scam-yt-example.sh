#/bin/bash
cam="$1" # 1st parameter, sports cam url, like rtsp://192.168.1.254/stream.mp4
alsain="$2" # 2nd parameter, name of ALSA input as obtained by arecord -L, like "my_Audio_8-DJ_(Ch-C)"
skey="$3" # 3rd parameter the streaming key obtained by logging into youtube and setting up streaming
# need to adjust itsoffset and volume depending on the setup
ffmpeg -thread_queue_size 8192 -re -i "$cam" -itsoffset 1.3 -thread_queue_size 8192 -ac 2 -f alsa -ar 44100 -re -i "$alsain" -c:a aac -b:a 192k -ar 44100 -ac 2 -af "volume=1" -c:v copy -threads 0 -map 0:0 -map 1:0  -f flv rtmp://a.rtmp.youtube.com/live2/rtmp/"$skey"
