#/bin/bash
img="$1" # 1st parameter, path to still image, like /home/demo/test.png
alsain="$2" # 2nd parameter, name of ALSA input as obtained by arecord -L, like "my_Audio_8-DJ_(Ch-C)"
skey="$3" # 3rd parameter the streaming key obtained by logging into youtube and setting up streaming
ffmpeg -thread_queue_size 8192 -r 25 -re -loop 1 -i "$img" -thread_queue_size 8192 -ac 2 -f alsa -ar 44100 -re -i "$alsain" -c:a aac -b:a 192k -ar 44100 -ac 2 -af "volume=2" -c:v libx264 -g 100 -crf 18 -vf "fps=25,format=yuv420p" -b:v 2500k -bufsize 1250k  -minrate 2500k -maxrate 2500k -s 1280x720 -threads 0 -map 0:0 -map 1:0 -r 25 -f flv rtmp://a.rtmp.youtube.com/live2/rtmp/"$skey"
