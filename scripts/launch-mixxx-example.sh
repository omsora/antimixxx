#!/bin/bash

if [ $(id -u) = 0 ]; then
   echo "Do not run this as root!" >&2
   exit 1
fi
set -xv
# conventionally the configuration files will be stored in the same dir as the script, but you can pick whatever full path, make sure it's preserved across reboots if you run in live/frugal modes!
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
mixxxcfg="$SCRIPTPATH/config.mixxx"
alsacfg="$SCRIPTPATH/config.asoundrc"

# optionally back up the config directory, warning, wasteful of space, a git repo would be a better solution
#cp "$mixxxcfg" "$mixxxcfg"$(date +%F-%T) -a 

# UNTESTED! optionally avoid spinning down the hd
#sudo hdparm -S 245 /dev/sda

# install cfg files
# install rtirq as a startup service, or start it from here, about configuring rtirq see https://www.mixxx.org/wiki/doku.php/adjusting_audio_latency
#rtirqdir="$SCRIPTPATH/rtirq"
#sudo cp "$rtirqdir"/rtirq.conf /etc/ && sudo "$rtirqdir"/rtirq.sh restart
cp "$alsacfg" ~/.asoundrc
sudo /etc/init.d/alsa-utils restart
ln -s "$mixxxcfg" ~/.mixxx # a symlink is enough


# optionally disable SMT
#sudo su -c "echo off > /sys/devices/system/cpu/smt/control"

# optionally raise CPU clock frequency, warning, eats battery and may heat things up, watch in your /sys how many scaling_governor files you have available and adjust cpuX number and quantity accordingly, note that disabling smt might make a difference
#sudo su -c "echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"
#sudo su -c "echo performance > /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor"


#finally run mixxx
cd ~ && mixxx
